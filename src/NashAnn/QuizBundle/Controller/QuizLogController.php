<?php

namespace NashAnn\QuizBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use NashAnn\QuizBundle\Entity\Question;
use NashAnn\QuizBundle\Entity\QuizLog;

class QuizLogController extends Controller
{
	/**
	 * Display Log index
	 * @return [type] [description]
	 */
	public function indexAction()
	{
		$em = $this->getDoctrine()->getManager();

		$logs = $em->getRepository('NashAnnQuizBundle:QuizLog')->findBy(
			array(),
			array('id' => 'ASC')
		);

		return $this->render('NashAnnQuizBundle:Front/Log:index.html.twig', array(
			'logs' 	=> $logs,
		));
	}

	/**
	 * delete a quiz log
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function deleteAction(Request $request)
	{
		if (!$logId = $request->request->get('id')) {
			throw new NotFoundHttpException("Log ID Invalid!");
		}

		$em = $this->getDoctrine()->getManager();
		if (!$log = $em->getRepository('NashAnnQuizBundle:QuizLog')->findOneById($logId)) {
			throw new NotFoundHttpException("Log Not Found!");
		}

		$em->remove($log);
		$em->flush();

		return new JsonResponse(array(
			'success' 	=> true,
			'message'	=> 'Log successfully deleted!',
		));
	}

	/**
     * Add user's answers
     * @param array $log [description]
     */
    public function addLogAction(Request $request)
    {
        $data = $request->request->get('data');

        //return new JsonResponse(array('size' => $data));

        $em     = $this->getDoctrine()->getManager();

        $log    = new QuizLog();
        $log->setAnswer($data);

        $em->persist($log);
        $em->flush();

        $logUrl =  $this->generateUrl('answer_log_view', array('id' => $log->getId()));

        return new JsonResponse(array('url' => $logUrl));
    }

    /**
     * View quiz log
     * @param  integer $id [description]
     * @return [type]      [description]
     */
    public function viewLogAction($id = 0)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$log = $em->getRepository('NashAnnQuizBundle:QuizLog')->findOneById($id)) {
            throw new NotFoundHttpException("Quiz Log Not Found!");
        }

        $totals = [
            'questions' => Question::QUESTION_QUANTITY,
            'correct'   => 0,
            'wrong'     => 0
        ];
        foreach ($log->getAnswer() as $question => $answer) {
           if (in_array('false', $answer)) {
                $totals['wrong']++;
                continue;
           }

           $totals['correct']++;
        }

        return $this->render('NashAnnQuizBundle:Front:log_view.html.twig', array(
            'log'       => $log,
            'totals'    => $totals,
        ));
    }

    public function reviewLogAction($id = 0)
    {
        if (!$id) {
            throw new NotFoundHttpException("Quiz ID Not Found!");
        }

        $em = $this->getDoctrine()->getManager();

        if (!$log = $em->getRepository('NashAnnQuizBundle:QuizLog')->findOneById($id)) {
            throw new NotFoundHttpException("Log Not Found!");
        }

        if (!$questions = $em->getRepository('NashAnnQuizBundle:Question')->findQuizResult($log)) {
            throw new NotFoundHttpException("Questions For This Quiz Are Missing!");
        }

        return $this->render('NashAnnQuizBundle:Front:review_log.html.twig', array(
            'logs'          => $log,
            'questions'     => $questions,
        ));
    }
}
