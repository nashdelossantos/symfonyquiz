<?php

namespace NashAnn\QuizBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\HttKernel\Exception\NotFoundHttpException;

use NashAnn\QuizBundle\Entity\Question;
use NashAnn\QuizBundle\Entity\QuizLog;

class QuizController extends Controller
{
	/**
	 * [indexAction description]
	 * @return [type] [description]
	 */
    public function indexAction()
    {
    	//$questions = $this->getDoctrine()->getRepository('NashAnnQuizBundle:Question')->findQuestionsForQuiz(Question::QUESTION_QUANTITY);
        $questions = $this->getDoctrine()->getRepository('NashAnnQuizBundle:Question')->findBy(array('category' => 'Routing', 'status' => 2));

        $questionSet = [];
        $sourceCheck = [];
        foreach ($questions as $question) {
            $questionId = $question->getId();

            $optionList = [];
            foreach ($question->getOptions() as $option) {
                $optionList[$option->getId()] = $option->getCorrect();
            }

            $sourceCheck[$questionId] = $optionList;

            // Shuffle Options
            $shuffleOptions = $question->getOptions()->toArray();
            shuffle($shuffleOptions);
            $currentQuestionSet = [
                'id'            => $question->getId(),
                'category'      => $question->getCategory(),
                'question'      => $question->getQuestion(),
                'isMultiple'    => $question->getIsMultiple(),
                'code'          => $question->getCode(),
                'language'      => $question->getLanguage(),
                'options'       => $shuffleOptions,
            ];
            $questionSet[] = $currentQuestionSet;
        }

        return $this->render('NashAnnQuizBundle:Front:index.html.twig', array(
        	'questions'		=> $questionSet,
            'sourceCheck'   => $sourceCheck
        ));
    }
}
