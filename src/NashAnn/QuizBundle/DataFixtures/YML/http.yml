category: HTTP
questions:
    -
        question: 'Which of these HTTP headers does not exist?'
        answers:
            - {value: Control-Cache,  correct: true}
            - {value: Cache-Modifier, correct: true}
            - {value: Expires,        correct: false}
            - {value: Last-Modified,  correct: false}
            - {value: Cache-Control,  correct: false}
    -
        question: 'Which one of these Response methods check if cache must be revalidated?'
        answers:
            - {value: $response->mustRevalidate(),    correct: true}
            - {value: $response->isRevalidated(),     correct: false}
            - {value: $response->getRevalidated(),    correct: false}
            - {value: $response->mustBeRevalidated(), correct: false}
    -
        question: 'Using a Response instance, which of these methods are available to check status code?'
        answers:
            - {value: $response->isInformational(), correct: true}
            - {value: $response->isSuccessful(),    correct: true}
            - {value: $response->isRedirection(),   correct: true}
            - {value: $response->isInvalid(),       correct: true}
            - {value: $response->isError(),         correct: false}
    -
        question: 'How to access the `foo` GET parameter from Request object $request ?'
        answers:
            - {value: "$request->query->get('foo');",     correct: true}
            - {value: "$request->request->get('foo');",   correct: false}
            - {value: "{$request->query->all()}['foo'];", correct: true}
            - {value: "$request->get('foo');",            correct: true}
    -
        question: 'How to access the `bar` POST parameter from Request object $request ?'
        answers:
            - {value: "$request->query->get('bar');",     correct: false}
            - {value: "$request->request->get('bar');",   correct: true}
            - {value: "{$request->query->all()}['baz'];", correct: false}
            - {value: "$request->post('bar');",           correct: false}
    -
        question: 'The method <code>getLanguages()</code> from Request object:'
        answers:
            - {value: 'return the value of the metadata lang',                   correct: false}
            - {value: 'return an array of languages available in translations',  correct: false}
            - {value: 'return an array of languages the client accepts',         correct: true}
            - {value: 'does not exists',                                         correct: false}
    -
        question: 'How to get the Content Type from Request ?'
        answers:
            - {value: "$request->headers->get('content_type');", correct: true}
            - {value: "$request->headers->get('content-type');", correct: true}
            - {value: "$request->headers->get('Content-Type');", correct: true}
            - {value: "$request->getContentType();",             correct: true}
    -
        question: 'How to check if a request have been sent using AJAX ?'
        answers:
            - {value: $request->isXmlHttpRequest();,                                   correct: true}
            - {value: $request->isAJAX();,                                             correct: false}
            - {value: "$this->headers->get('X-Requested-With') === 'XMLHttpRequest';", correct: true}
    -
        question: 'Theses Response subclasses are available: true or false ?'
        answers:
            - {value: JsonResponse,       correct: true}
            - {value: StreamResponse,     correct: false}
            - {value: BinaryFileResponse, correct: true}
            - {value: XmlResponse,        correct: false}
    -
        question: 'Which HTTP status code should for a resource that moved temporarily ?'
        answers:
            - {value: 301, correct: false}
            - {value: 302, correct: true}
            - {value: 201, correct: false}
            - {value: 204, correct: false}
    -
        question: 'True or False ? Server returns an 403 HTTP status code when you are not allowed to access a resource'
        answers:
            - {value: "True",    correct: true}
            - {value: "False",   correct: false}
    -
        question: 'Which class of HTTP status codes is used to indicate a client error?'
        answers:
            - {value: '1xx class',                                                      correct: false}
            - {value: '3xx class',                                                      correct: false}
            - {value: '4xx class',                                                      correct: true}
            - {value: '4xx class or 5xx classes, depending on the type of the error',   correct: false}
    -
        question: 'Which of the following sentences about the HTTP protocol are true?'
        answers:
            - {value: 'It''s a stateless protocol',                      correct: true}
            - {value: 'It''s a binary-based protocol',                   correct: false}
            - {value: 'GET, POST and DELETE are idempotent methods',     correct: false}
            - {value: 'GET, HEAD and OPTIONS are safe methods',          correct: true}
    -
        question: 'What is the purpose of the TRACE method?'
        answers:
            - {value: 'Performance',    correct: false}
            - {value: 'Debugging',      correct: true}
            - {value: 'Security',       correct: false}
            - {value: 'Caching',        correct: false}
    -
        question: 'Given the following HTTP request, what is the value of <code>Request::getScheme()</code>?'
        language: 'json'
        code: |
            GET / HTTP/1.1
            Host: localhost:8000
            Accept: text/html
            X-Scheme: tcp
        language: 'html'
        answers:
            - {value: 'tcp',        correct: false}
            - {value: 'localhost',  correct: false}
            - {value: '8000',       correct: false}
            - {value: 'http',       correct: true}
    -
        question: 'If you go to <code>https://example.com</code>, what is the port that the webserver is listening to?'
        answers:
            - {value: '21',     correct: false}
            - {value: '80',     correct: false}
            - {value: '443',    correct: true}
            - {value: '8080',   correct: false}
    -
        question: 'Are responses to the OPTIONS method cacheable?'
        answers:
            - {value: 'Yes',  correct: false}
            - {value: 'No',   correct: true}
    -
        question: 'Which of the following steps must be done in order to serve gzipped responses from Symfony apps?'
        answers:
            - {value: 'Call to the setCompression() method of the Response object, passing the value gzip',                                                                                          correct: false}
            - {value: 'Check if application/gzip is in the array returned by the getAcceptableContentTypes() method of the Request object, and in that case, gzip the contents with gzencode()',     correct: false}
            - {value: 'Set the option framework.request.compression to gzip.',                                                                                                                       correct: false}
            - {value: 'Nothing must be done, this is handled by the web server.',                                                                                                                    correct: true}
    -
        question: 'If the webserver returns the CSS file styles.css compressed with GZIP, what is the value of the Content-type header?'
        answers:
            - {value: 'application/gzip',   correct: false}
            - {value: 'text/css',           correct: true}
            - {value: 'text/css+gzip',      correct: false}
            - {value: 'deflate',            correct: false}
    -
        question: 'How does the browser communicate to the server what is the preferred language of the user?'
        answers:
            - {value: 'The main language is included in the User-Agent header',                                                                                 correct: false}
            - {value: 'User languages preferences are sent using the Accept-Language header',                                                                   correct: true}
            - {value: 'The main language is sent using the Locale header, while secondary languages are usually included in the X-Locale-Alternate header.',    correct: false}
            - {value: 'The browser doesn’t communicate the language of the user, it’s inferred from the user IP',                                               correct: false}
    -
        question: 'Given the following controller, what is the content returned by a HEAD request?'
        code: |
            AppBundle/Controller/BookController.php

            use Symfony\\Component\\HttpFoundation\\Request;
            use Symfony\\Component\\HttpFoundation\\Response;

            class BookController {
                public function indexAction(Request $request) {
                    return new Response($request->getHttpHost());
                }
            }
        answers:
            - {value: 'HEAD',               correct: false}
            - {value: 'GET',                correct: false}
            - {value: 'HTTP',               correct: false}
            - {value: 'Empty  Response',    correct: true}
    -
        question: 'Which of the following sentences are true about the DELETE method?'
        answers:
            - {value: 'It asks the server to delete a resource',        correct: true}
            - {value: 'It may not be available in all browsers',        correct: true}
            - {value: 'It must return 105 Deleted as status code.',     correct: false}
            - {value: 'It was removed in HTTP 1.1',                     correct: false}
    -
        question: 'What HTTP status code would you return if http://example.com/1 is not available temporarily and want to redirect all requests to http://example.com/2 until it gets back?'
        answers:
            - {value: '200',    correct: false}
            - {value: '204',    correct: false}
            - {value: '301',    correct: false}
            - {value: '302',    correct: true}
    -
        question: 'In the HTTP protocol, can the same URI accept more than one method?'
        answers:
            - {value: 'Yes',    correct: true}
            - {value: 'No',     correct: false}
    -
        question: 'What exception must be thrown in Symfony to generate a HTTP 404 status code?'
        answers:
            - {value: 'Symfony\Component\HttpKernel\Exception\NotFoundHttpException',           correct: true}
            - {value: 'Symfony\Component\HttpKernel\Exception\ConflictHttpException',           correct: false}
            - {value: 'Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException',   correct: false}
            - {value: 'Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException',       correct: false}
