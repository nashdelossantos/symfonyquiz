category: Security
questions:
    -
        question: 'Which line is correct to add a security.access_control line?'
        answers:
            - {value: "- { path: ^/admin, roles: ROLE_ADMIN }", correct: true}
            - {value: "- { path: ^/admin, acl: ROLE_ADMIN }",   correct: false}
            - {value: "- { url: ^/admin, roles: ROLE_ADMIN }",  correct: false}
            - {value: "- { url: ^/admin, acl: ROLE_ADMIN }",    correct: false}
    -
        question: 'After a login success, what is the parameter name to redirect on referer URL?'
        answers:
            - {value: security.firewalls.<name>.form_login.use_referer,         correct: true}
            - {value: security.firewalls.<name>.form_login.after_login_referer, correct: false}
            - {value: security.firewalls.<name>.form_login.referer,             correct: false}
            - {value: security.firewalls.<name>.form_login.success_referer,     correct: false}
    -
        question: 'In Symfony Security component:'
        answers:
            - {value: 'Symfony\Bundle\SecurityBundle\Security\FirewallContext services are publicly created that correspond to each context you create under your security.yml, a context is called an authenticator', correct: false}
            - {value: 'Symfony\Bundle\SecurityBundle\Security\FirewallContext services are privately created that correspond to each context you create under your security.yml, a context is also known as per each firewall', correct: false}
            - {value: 'Symfony\Bundle\SecurityBundle\Security\FirewallContext services are synthetically created that correspond to each context you create under your security.yml, each context is liked to an authenticator', correct: false}
            - {value: 'Symfony\Bundle\SecurityBundle\Security\FirewallContext services are publicly created that correspond to each context you create under your security.yml, a context is related to each FirewallContext directly but is overall handled by a generic Symfony\Bundle\SecurityBundle\Security\Firewall object', correct: true}
    -
        question: 'Is user authenticated in all of security firewalls after a successful login:'
        answers:
            - {value: 'Yes, it happens automatically', correct: false}
            - {value: 'Yes, if firewalls have the same value of the `context` option', correct: true}
            - {value: 'Yes, if option `shared` is set to true', correct: false}
            - {value: 'No, it is not possible, firewalls are independent from each other', correct: false}
    -
        question: 'How to force a secure area to use the HTTPS protocol in the security config?'
        answers:
            - {value: 'access_control: { path: ^/secure, requires_channel: https }', correct: true}
            - {value: 'access_control: { path: ^/secure, use_https: true }', correct: false}
            - {value: 'access_control: { path: ^/secure, always_use_https: true }', correct: false}
            - {value: 'access_control: { path: ^/secure, schemes: [https] }', correct: false}
    -
        question: 'What is the purpose of security encoders in security.yml?'
        answers:
            - {value: 'Encode user passwords using given algorithm', correct: true}
            - {value: 'Encrypt a HTTP response', correct: false}
            - {value: 'Encode all data in the application database', correct: false}

    -
        question: 'Which authentication events exist in the Security component?'
        answers:
            - {value: 'security.authentication.success', correct: true}
            - {value: 'security.authentication.failure', correct: true}
            - {value: 'security.interactive_login', correct: true}
            - {value: 'security.login', correct: false}
            - {value: 'security.switch_user', correct: true}
            - {value: 'security.authentication.start', correct: false}
            - {value: 'security.remember_me_login', correct: false}
    -
        question: 'What does the default strategy `affirmative` of the access decision manager mean?'
        answers:
            - {value: 'Access is granted as soon as there is one voter granting access', correct: true}
            - {value: 'Access is granted if there are more voters granting access than denying', correct: false}
            - {value: 'Access is granted if all voters grant acces', correct: false}
            - {value: 'Access is granted if no voters throw an exception', correct: false}
    -
        question: By default, how does Symfony know that the file app/config/security.yml contains the security system configuration?
        answers:
            - {value: 'Byconvention, Symfony automatically looks for a file called security.{yml|xml|php} in that directory',   correct: false}
            - {value: It is loaded from the main configuration file,                                                            correct: true}
            - {value: It is loaded directly from the AppKernel class,                                                           correct: false}
            - {value: The option framework.security. file contains the relative path of the file,                               correct: false}
    -
        question: What is the purpose of the following firewall defined in the security.yml file?
        language: 'yml'
        code: |
            # app/config/security.yml
            security:
                firewalls:
                    dev:
                        pattern: ^/(_(profiler|wdt)|css|images|js)/
                        security: false
        answers:
            - {value: Disable the security system for the dev environment.,                     correct: false}
            - {value: Disable Symfony development tools for the dev environment.,               correct: false}
            - {value: Avoid Symfony development tools being blocked by the security system.,    correct: true}
            - {value: Enable the profiler to be used in functional tests.,                      correct: false}
    -
        question: When using HTTP basic, how does the server starts the authentication process?
        answers:
            - {value: Rendering a login form with the fields_user and_password.,                                correct: false}
            - {value: Sending the WWW-Authenticate HTTP header with the HTTP 401 Not Authorized status code.,   correct: true}
            - {value: Sending the status code HTTP 418 Authentication Required.,                                correct: false}
            - {value: Redirecting the request to the port 443.,                                                 correct: false}
    -
        question: Which of the following authentication methods are not included by default in the Symfony security component?
        answers:
            - {value: HTTP basic.,  correct: false}
            - {value: Form login,   correct: false}
            - {value: OAuth.,       correct: true}
            - {value: OpenID.,      correct: true}
    -
        question: Is it possible to secure services using security.context?
        answers:
            - {value: 'Yes',                                                                        correct: true}
            - {value: 'Yes, but only when the service does not make use of the Request object.',    correct: false}
            - {value: 'No',                                                                         correct: false}
    -
        question: How can you get the User object of the current user from a controller extending from the base controller?
        answers:
            - {value: '$this->user',                                            correct: false}
            - {value: "$this->get('security.context')->getToken()->getUser()",  correct: true}
            - {value: '$this->getSecurity()->getUser()',                        correct: false}
            - {value: '$this->getUser()',                                       correct: true}
    -
        question: What is the length of the string generated by the password_hash() function when using the PASSWORD_BCRYPT algorithm?
        answers:
            - {value: 32, correct: false}
            - {value: 40, correct: false}
            - {value: 42, correct: false}
            - {value: 60, correct: true}
    -
        question: Given the following security.yml file, how the password of the user user will be stored in the database?
        language: 'yml'
        code: |
            # app/config/security.yml
            security:
                encoders:
                    Symfony\Component\Security\Core\User\User: plaintext
                providers:
                    in_memory:
                        memory:
                            users:
                                user:  { password: 1234, roles: [ 'ROLE_USER' ] }
        answers:
            - {value: In plain text,                 correct: false}
            - {value: Hashed with md5,               correct: false}
            - {value: Hashed with bcrypt,            correct: false}
            - {value: None of the above are correct, correct: true}
    -
        question: Which of the following sentences are true when using a password encoder based on algorithms like md5, sha1 or bcrypt?
        answers:
            - {value: Passwords are stored in the database encoded,                                                                             correct: true}
            - {value: You can change the hashing algorithm at any time and authentication should keep working as long as salts are rehashed,    correct: false}
            - {value: 'Authentication works by comparing both hashed passwords, the one stored in the database and one provided by the user',   correct: true}
            - {value: 'Given the same password, salt and algorithm, you will get a different hash string in each execution',                    correct: false}
    -
        question: What PHP function can you use to know if the joaat algorithm is available to encode passwords?
        answers:
            - {value: hash(),           correct: false}
            - {value: hash_algos(),     correct: true}
            - {value: is_hash(),        correct: false}
            - {value: hash_joaat(),     correct: false}
    -
        question: Given the following role hierarchy, does the isGranted() method of the security.context service return true for both ROLE_ADMIN and ROLE_USER roles, if the user has the ROLE_ADMIN role?
        language: 'yml'
        code: |
            # app/config/security.yml
            security:
                role_hierarchy:
                    ROLE_ADMIN: ROLE_USER
        answers:
            - {value: 'Yes', correct: true}
            - {value: 'No',  correct: false}
    -
        question: Would a user without the ROLE_ADMIN role have access to /profile/admin, assuming the following access_control expressions?
        language: yml
        code: |
            # app/config/security.yml
            security:
                access_control:
                    - { path: /admin, roles: ROLE_ADMIN }
        answers:
            - {value: 'Yes', correct: false}
            - {value: 'No',  correct: true}
    -
        question: Is it recommended to encode passwords in the database when using HTTPS?
        answers:
            - {value: 'Yes',                                                            correct: true}
            - {value: 'No, as they are already encrypted',                              correct: false}
            - {value: 'No, as there is no way to get the password entered by the user', correct: false}
            - {value: 'None of the above are correct',                                  correct: false}
    -
        question: Do you see any potential problem if a registration form returns the following error? “The password you entered cannot be longer than 60 characters”
        answers:
            - {value: 'Yes, they might be using a weak encryption algorithm',   correct: false}
            - {value: 'Yes, they might be storing the passwords in plain text', correct: true}
            - {value: 'Yes, they might be using an old version of PHP',         correct: false}
            - {value: 'No',                                                     correct: false}
    -
        question: Do all logged in users have the IS_AUTHENTICATED_REMEMBERED “role”?
        answers:
            - {value: 'No, only those logged in because of a “remember me cookie”', correct: false}
            - {value: 'No, only those logged in because of login form',             correct: false}
            - {value: 'No, only anonymous users have this',                         correct: false}
            - {value: 'Yes',                                                        correct: true}
    -
        question: If there are 5 voters to decide whether a user has granted access to an action and the strategy is afirmative, how many voters must return VoterInterface::ACCESS_GRANTED to grant access?
        answers:
            - {value: '0',       correct: false}
            - {value: 1 or more, correct: true}
            - {value: 3 or more, correct: false}
            - {value: 5,         correct: false}
    -
        question: Which of the following sentences about ACLs are true?
        answers:
            - {value: 'The init:acl command generates ACL tables in the database',  correct: true}
            - {value: They are simpler to use than voters,                          correct: false}
            - {value: The order in which entries are checked is important,          correct: true}
            - {value: ACEs are checked explicitly in the controller,                correct: false}
    -
        question: ACEs are checked explicitly in the controller
        answers:
            - {value: 'Yes', correct: false}
            - {value: 'No',  correct: true}