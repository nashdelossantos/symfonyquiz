<?php

namespace NashAnn\QuizBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

use NashAnn\QuizBundle\Entity\Question;
use NashAnn\QuizBundle\Entity\Answer;


class LoadQuestionData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {

        $finder = new Finder();
        $finder->files()->in('src/NashAnn/QuizBundle/DataFixtures/YML');

        foreach ($finder as $file) {
            $contents = $file->getContents();
            try {
                $questions = Yaml::parse($contents);
            } catch (Exception $e) {
                $questions = null;
            }

            if (is_array($questions)) {

                foreach ($questions['questions'] as $question) {
                    $questionObj = new Question();
                    $questionObj->setCategory($questions['category']);
                    $questionObj->setQuestion($question['question']);

                    var_dump($questions['category']);
                    var_dump($question['question']);

                    if (isset($question['code'])) {
                        $questionObj->setCode($question['code']);
                    }

                    if (isset($question['language'])) {
                        $questionObj->setLanguage($question['language']);
                    }

                    $optionTypeValues = array();
                    foreach ($question['answers'] as $option) {
                        $answer = new Answer();
                        $answer->setOption($option['value']);
                        $answer->setCorrect($option['correct']);
                        $answer->setQuestion($questionObj);
                        //var_dump($option['value']);
                        $manager->persist($answer);

                        $questionObj->addOption($answer);

                        $optionTypeValues[] = $option['correct'] ? 1 : 0;
                    }

                    $optionValuesCount = array_count_values($optionTypeValues);
                    $isMultiple        = $optionValuesCount[1];

                    if ($isMultiple > 1) {
                        $questionObj->setIsMultiple(true);
                    }

                    $manager->persist($questionObj);

                }
            }

            $manager->flush();
        }
    }
}