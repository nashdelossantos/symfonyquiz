<?php

namespace NashAnn\QuizBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QuizLog
 *
 * @ORM\Table(name="quiz_log")
 * @ORM\Entity(repositoryClass="NashAnn\QuizBundle\Repository\QuizLogRepository")
 */
class QuizLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="answer", type="object")
     */
    private $answer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;


    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return QuizLog
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set answer
     *
     * @param array $answer
     *
     * @return QuizLog
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return array
     */
    public function getAnswer()
    {
        return $this->answer;
    }
}
