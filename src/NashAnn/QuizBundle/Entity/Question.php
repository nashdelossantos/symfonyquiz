<?php

namespace NashAnn\QuizBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Question
 *
 * @ORM\Table(name="question")
 * @ORM\Entity(repositoryClass="NashAnn\QuizBundle\Repository\QuestionRepository")
 */
class Question
{
    // << Statuses
    const STATUS_DRAFT      = 1;
    const STATUS_PUBLISHED  = 2;
    const STATUS_HAS_ERROR  = 3;

    public function getStatuses()
    {
        return [
            self::STATUS_DRAFT      => 'Draft',
            self::STATUS_PUBLISHED  => 'Published',
            self::STATUS_HAS_ERROR  => 'Error'
        ];
    }

    public function getStatusName($status = 0)
    {
        return [array_key_exists($status, $this->getStatuses()) ? $this->getStatuses()[$status] : null];
    }
    // >> Statuses

    const QUESTION_QUANTITY = 100;
    const DEFAULT_LANGUAGE  = 'php';

    const AUTO_FEEDBACK     = true;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="category", type="string", length=90, nullable=true)
     */
    private $category;

    /**
     * @ORM\Column(name="is_multiple", type="boolean")
     */
    private $isMultiple;

    /**
     * @ORM\Column(name="question", type="text")
     */
    private $question;

    /**
     * @ORM\Column(name="code", type="text", nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(name="language", type="string", length=30, nullable=true)
     */
    private $language;

    /**
     * @ORM\OneToMany(targetEntity="Answer", mappedBy="question", cascade={"all"})
     */
    private $options;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     *
     * @return Question
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
    * Get code
    * @return
    */
    public function getCode()
    {
        return $this->code;
    }

    /**
    * Set code
    * @return $this
    */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Question
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Question
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Question
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created      = new \DateTime();
        $this->updated      = new \DateTime();
        $this->status       = self::STATUS_PUBLISHED;
        $this->isMultiple   = false;
        $this->options      = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add option
     *
     * @param \NashAnn\QuizBundle\Entity\Answer $option
     *
     * @return Question
     */
    public function addOption(\NashAnn\QuizBundle\Entity\Answer $option)
    {
        $this->options[] = $option;

        return $this;
    }

    /**
     * Remove option
     *
     * @param \NashAnn\QuizBundle\Entity\Answer $option
     */
    public function removeOption(\NashAnn\QuizBundle\Entity\Answer $option)
    {
        $this->options->removeElement($option);
    }

    /**
     * Get options
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return Question
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set isMultiple
     *
     * @param boolean $isMultiple
     *
     * @return Question
     */
    public function setIsMultiple($isMultiple)
    {
        $this->isMultiple = $isMultiple;

        return $this;
    }

    /**
     * Get isMultiple
     *
     * @return boolean
     */
    public function getIsMultiple()
    {
        return $this->isMultiple;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return Question
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        if (!$this->language) {
            $this->language = self::DEFAULT_LANGUAGE;
        }

        return $this->language;
    }

    public function getAutoFeedback()
    {
        return self::AUTO_FEEDBACK;
    }
}
