var logPath 	= null,
	currentQ  	= 0,
	$answers 	= {},

	correctShown = false,
	wrongShown 	 = false,
	totalCorrect = 0,
	totalWrong   = 0;

// << Instantiate Question Assets
function initQuestionAssets(answerBank, path) {
	var $submitQuiz   	= $('#submitQuiz');

	logPath = path;

	initICheck(answerBank, $submitQuiz);
	initSlickerSlider($submitQuiz);
	initTimer();
}

// instantiate Slicker JS
function initSlickerSlider($submitQuiz) {
	//slider
	var $slickElement = $('.q-items'),
		$quizElements = $('.q-item'),
		$slickNext    = $('.slick-next');

	$slickElement.slick({
		prevArrow: false,
		speed: 100,
		dots: false,
		fade: true,
		infinite: false,
		adaptiveHeight: true,
		draggable: false,
		swipe: false
	});
	$slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
	    var i = (currentSlide ? currentSlide : 1) + 1;
	    if (i == slick.slideCount) {
	    	$submitQuiz.removeClass('hidden');
	    }

	    $('#locator .first').text(i);
	});

	$slickElement.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
		$('.smk-alert').remove();
	});

	$slickElement.on('afterChange', function(event, slick, currentSlide, nextSlide) {
		correctShown = false,
		wrongShown 	 = false,
		totalCorrect = 0,
		totalWrong   = 0;
	});
}

// instantiate iCheck JS
function initICheck(answerBank, $submitQuiz) {
	var iCheckboxElement = $("input[type='checkbox']"),
		iRadioboxElement = $("input[type='radio']");

	iCheckboxElement.iCheck({
	    checkboxClass: 'icheckbox_square'
	});
	iRadioboxElement.iCheck({
		radioClass: 'iradio_square'
	});

	// << User ticks an option
	iRadioboxElement.on('ifChecked', function(e) {
		var $this 				= $(this),
			questionId			= $this.data('qid'),
			optionId 			= $this.data('id')
		;


		var option 				= { [optionId] : false};
		$answers[questionId] 	= option;

		currentQ 				= questionId;

		notify(answerBank[questionId][optionId]);
	});

	iCheckboxElement.on('ifChecked', function(e) {
		var $this 				= $(this),
			questionId			= $this.data('qid'),
			optionId 			= $this.data('id')
		;

		var option 				= { [optionId] : false};
		if (questionId in $answers) {
			$answers[questionId][optionId] = false;
		} else {
			$answers[questionId] 	= option;
		}

		currentQ 				= questionId;

		notify(answerBank[questionId][optionId], true);
	});

	iCheckboxElement.on('ifUnchecked', function(e) {
		var $this 				= $(this),
			questionId			= $this.data('qid'),
			optionId 			= $this.data('id')
		;

		delete $answers[questionId][optionId];

		if ($.isEmptyObject($answers[questionId])) {
			delete $answers[questionId];
		}

		unNotify(answerBank[questionId][optionId]);
	});

	$submitQuiz.click(function(e) {
		e.preventDefault();
		calculateResults(answerBank);
	});
}

function notify(isCorrect, isMulti)
{

	$('.smk-alert').remove();

	if (isMulti) {
		if (isCorrect) {
			totalCorrect++;
		} else {
			totalWrong++;
		}

		if (totalWrong > 0 ) {
			showNotification('At least 1 is wrong', 'danger');
		} else {
			showNotification('Correct', 'success');
		}
	} else {
		if (isCorrect) {
			showNotification('Correct', 'success');
			return;
		}

		showNotification('Wrong', 'danger');
	}
}

function showNotification(text, type)
{
	$.smkAlert({
		text: text,
	    type: type,
	    position:'top-left',
	    permanent: true
	});
	return;
}

function unNotify(isCorrect)
{
	$('.smk-alert').remove();

	if (isCorrect) {
		totalCorrect--;
	} else {
		totalWrong--;
	}

	if (totalWrong == 0 && totalCorrect == 0) {
		return;
	}

	if (totalWrong > 0 ) {
		showNotification('At least 1 is wrong', 'danger');
	} else {
		showNotification('Correct', 'success');
	}

}

// << Calculate Results
function calculateResults(answerBank) {
	$.each(answerBank, function(bankId, options) {
		var isMultiple 		= false,
			correctCounter	= 0;

		$.each(options, function(optionId, correct) {
			if (correct) {
				currAnswer = $answers[bankId];

				for (a in currAnswer) {
					if (a == optionId) {
						$answers[bankId][optionId] = true;
						continue;
					}
				}
			}
		});
	});

	$.post(logPath, {data: $answers}, function(response) {
		if (response.url) {
			window.location.href = response.url;
		}
	});
}
// >> Calculate Results

// << Timer
function initTimer() {
	var timerText = $('#timer');
	var d 		= new Date();
		d.setHours(d.getHours() +1);
	var timeLeft =   d.getFullYear() + '/' +
					('0' + (d.getMonth()+1)).slice(-2) + '/' +
					('0' + (d.getDate())).slice(-2) + ' ' +
					d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();

	timerText.countdown(timeLeft, { elapse: false })
		.on('update.countdown', function(event) {
			var hours 	= event.offset.hours,
				minutes = event.offset.minutes,
				seconds = event.offset.seconds;

			var formatCounter = "%H hour %M minutes %S seconds";

			if (hours == 0) {
				formatCounter = "%M minutes %S seconds";
			}
			if (minutes == 0) {
				formatCounter = "%S seconds";
			}
			timerText.html(event.strftime(formatCounter));
	    });
}
// >> Timer

//  << Log Index
$.fn.deleteLog = function(id) {
	var _this = $(this),
		path = _this.data('path');

	swal({
		title: "Are you sure?",
		text: "You will not be able to recover this log",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, delete it!",
		closeOnConfirm: false,
		html: false
	}, function(){

		$.post(path, {id: id}, function(event) {
			if (event.success) {
				swal("Deleted!",
				event.message,
				"success");

				$('#logRow-'+id).remove();

				return;
			}

			swal({
				title: "Error!",
				text: "There was an error deleting the Log",
				type: "warning",
				showCancelButton: false,
				closeOnConfirm: true
			});
		});
	});
}
//  >> Log Index